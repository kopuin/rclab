extern crate portaudio_rs as portaudio;

use std::ffi::CString;

mod rawcpp;
mod rawcwrap;

fn main() {

  test_cwrap();
  test_cpp();

}


fn test_cwrap() {
  unsafe {
    let some = CString::new("some").unwrap();
    let psome = some.as_ptr();
    std::mem::forget(psome);

    let other = CString::new("other").unwrap();
    let pother = other.as_ptr();
    std::mem::forget(pother);

    rawcwrap::root::SnowboyDetectConstructor(psome, pother);
  }
}

fn test_cpp() {
  unsafe {
    let some = CString::new("some").unwrap();
    let psome = some.as_ptr() as *const rawcpp::root::std::string;
    std::mem::forget(psome);

    let other = CString::new("other").unwrap();
    let pother = other.as_ptr() as *const rawcpp::root::std::string;
    std::mem::forget(pother);

    rawcpp::root::snowboy::SnowboyDetect::new(psome, pother);
  }
}
