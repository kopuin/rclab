extern crate cc;
extern crate bindgen;

use std::path::PathBuf;


fn main() {


  println!("cargo:rustc-link-search=./snowboy/lib/ubuntu64");

  println!("cargo:rustc-link-lib=static=stdc++");

  /*

  // snowboy example C make output

  g++ -D_GLIBCXX_USE_CXX11_ABI=0 -fPIC \
      -I../../ -std=c++0x -Wall -Wno-sign-compare -Wno-unused-local-typedefs \
      -Winit-self -rdynamic -DHAVE_POSIX_MEMALIGN -Iportaudio/install/include -O3 \
      -c -o snowboy-detect-c-wrapper.o snowboy-detect-c-wrapper.cc


  // cc::Build output
  "c++" "-O0" "-ffunction-sections" "-fdata-sections" "-fPIC" \
      "-g" "-fno-omit-frame-pointer" "-m64" "-I" "snowboy" "-Wall" \
      "-Wextra" "-c" "-lstdc++" "-D_GLIBCXX_USE_CXX11_ABI=0" "-fPIC" "-std=c++0x" \
      "-Wall" "-Wno-sign-compare" "-Wno-unused-local-typedefs" "-Winit-self" \
      "-rdynamic" "-DHAVE_POSIX_MEMALIGN" "-O3" \
      "-o" "/opt/data/dev/rust/mei/target/debug/build/mei-ac09675cc2103be8/out/snowboy/c/snowboy-detect-c-wrapper.o" \
      "-c" "snowboy/c/snowboy-detect-c-wrapper.cc"

  */

  cc::Build::new()
    .cpp(true)
    .file("snowboy/c/snowboy-detect-c-wrapper.cc")
    .include("snowboy")
    .flag("-c")
    .flag("-lstdc++")
    .flag("-D_GLIBCXX_USE_CXX11_ABI=0")
    .flag("-fPIC")
    .flag("-std=c++0x")
    .flag("-Wall")
    .flag("-Wno-sign-compare")
    .flag("-Wno-unused-local-typedefs")
    .flag("-Winit-self")
    .flag("-rdynamic")
    .flag("-DHAVE_POSIX_MEMALIGN")
    .flag("-O3")
    .compile("libsnowboy-detect-c-wrapper.a");


  // Tell cargo to tell rustc to link the system bzip2
  // shared library.
  println!("cargo:rustc-link-lib=bz2");

  let bindings = bindgen::Builder::default()
    .header("./snowboy/c/snowboy-detect-c-wrapper.h")
    .enable_cxx_namespaces()
    .layout_tests(false)
    .opaque_type("std::.*")
    .generate()
    .expect("Unable to generate bindings");

  bindings
    .write_to_file(PathBuf::from("./src/rawcwrap.rs").as_path())
    .expect("Couldn't write bindings!");




  let bindings = bindgen::Builder::default()
    .header("./snowboy/include/snowboy-detect.h")
    .enable_cxx_namespaces()
    .layout_tests(false)
    .clang_arg("-v")
    .clang_arg("-x")
    .clang_arg("c++")
    .clang_arg("-std=c++11")
    .opaque_type("std::.*")
    .whitelist_type("snowboy::.*")
    .whitelist_function("snowboy::.*")
    .generate()
    .expect("Unable to generate bindings");

  bindings
    .write_to_file(PathBuf::from("./src/rawcpp.rs").as_path())
    .expect("Couldn't write bindings!");

}
